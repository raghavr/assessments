'use strict';

const express = require('express');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/status', (req, res) => {
  res.json({version:,
            commithashsha:,
            description: "Default JSON response"
          });
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}/status`);