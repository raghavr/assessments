This solution focus on deploying a NODE JS application into a Dockerfile

 

  - The application has a single endpoint "/status".

  - The application runs on ports 80 mapped to container port 8080

  - http://localhost/status URL returns output in the JSON format containing Application Version, Last Commit SHA and Description.

 

# Files in this Solution

 

  - server.js

  - Dockerfile

  - package.json

  - .gitlab-ci.yml